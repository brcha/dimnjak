#include <iostream>
#include <cstdio>
#include <cmath>

#include "gsl/gsl_integration.h"

double f(double x, void*) {
    return std::log(x)/std::sqrt(x);
}

int main(int argc, char* argv[]){
    gsl_integration_workspace* w = gsl_integration_workspace_alloc(1000);

    double result, error;
    double expected = -4.0;

    gsl_function F;
    F.function = &f;

    // gsl_integration_qag(&F, 0, 1, 0, 1e-7, 1000
    //                    ,GSL_INTEG_GAUSS61, w, &result, &error);

    gsl_integration_qags(&F, 0, 1, 0, 1e-7, 1000, w, &result, &error);

    printf ("result          = % .18f\n", result);
    printf ("exact result    = % .18f\n", expected);
    printf ("estimated error = % .18f\n", error);
    printf ("actual error    = % .18f\n", result - expected);
    printf ("intervals       = %zu\n", w->size);

    gsl_integration_workspace_free(w);

    return 0;
}